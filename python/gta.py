#!/usr/bin/env python3

import sys
import click


@click.group()
def cli():
    """Welcome. I'm the Git Teaching Aide."""


@cli.command()
def list():
    click.echo("You requested the list command")


if __name__ == "__main__":
    sys.exit(cli())
